/*
* Param_SysParam_Type.cs
*
* 功 能： N/A
* 类 名： Param_SysParam_Type
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:53:47    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Collections.Generic;
using System.Data;
using XHD.Common;

namespace XHD.BLL
{
    /// <summary>
    ///     Param_SysParam_Type
    /// </summary>
    public class Param_SysParam_Type
    {
        private readonly DAL.Param_SysParam_Type dal = new DAL.Param_SysParam_Type();

        #region  Method

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public void Add(Model.Param_SysParam_Type model)
        {
            dal.Add(model);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.Param_SysParam_Type model)
        {
            return dal.Update(model);
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete()
        {
            //该表无主键信息，请自定义主键/条件字段
            return dal.Delete();
        }

        /// <summary>
        ///     得到一个对象实体
        /// </summary>
        public Model.Param_SysParam_Type GetModel()
        {
            //该表无主键信息，请自定义主键/条件字段
            return dal.GetModel();
        }

        /// <summary>
        ///     得到一个对象实体，从缓存中
        /// </summary>
        public Model.Param_SysParam_Type GetModelByCache()
        {
            //该表无主键信息，请自定义主键/条件字段
            string CacheKey = "Param_SysParam_TypeModel-";
            object objModel = DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel();
                    if (objModel != null)
                    {
                        int ModelCache = ConfigHelper.GetConfigInt("ModelCache");
                        DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch
                {
                }
            }
            return (Model.Param_SysParam_Type) objModel;
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public List<Model.Param_SysParam_Type> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public List<Model.Param_SysParam_Type> DataTableToList(DataTable dt)
        {
            var modelList = new List<Model.Param_SysParam_Type>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                Model.Param_SysParam_Type model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new Model.Param_SysParam_Type();
                    if (dt.Rows[n]["id"] != null && dt.Rows[n]["id"].ToString() != "")
                    {
                        model.id = int.Parse(dt.Rows[n]["id"].ToString());
                    }
                    if (dt.Rows[n]["params_name"] != null && dt.Rows[n]["params_name"].ToString() != "")
                    {
                        model.params_name = dt.Rows[n]["params_name"].ToString();
                    }
                    if (dt.Rows[n]["params_order"] != null && dt.Rows[n]["params_order"].ToString() != "")
                    {
                        model.params_order = int.Parse(dt.Rows[n]["params_order"].ToString());
                    }
                    if (dt.Rows[n]["isDelete"] != null && dt.Rows[n]["isDelete"].ToString() != "")
                    {
                        model.isDelete = int.Parse(dt.Rows[n]["isDelete"].ToString());
                    }
                    if (dt.Rows[n]["Delete_time"] != null && dt.Rows[n]["Delete_time"].ToString() != "")
                    {
                        model.Delete_time = DateTime.Parse(dt.Rows[n]["Delete_time"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder, out Total);
        }

        #endregion  Method
    }
}