/*
* CRM_order_details.cs
*
* 功 能： N/A
* 类 名： CRM_order_details
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:53:47    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;

namespace XHD.BLL
{
    /// <summary>
    ///     CRM_order_details
    /// </summary>
    public class CRM_order_details
    {
        private readonly DAL.CRM_order_details dal = new DAL.CRM_order_details();

        #region  Method

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public void Add(Model.CRM_order_details model)
        {
            dal.Add(model);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.CRM_order_details model)
        {
            return dal.Update(model);
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(string wherestr)
        {
            //该表无主键信息，请自定义主键/条件字段
            return dal.Delete(wherestr);
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }


        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        #endregion  Method
    }
}