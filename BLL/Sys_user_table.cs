﻿/*
* Sys_user_table.cs
*
* 功 能： N/A
* 类 名： Sys_user_table
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015/8/1 10:38:12    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com  www.xhdoa.com. All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;

namespace XHD.BLL
{
    /// <summary>
    ///     Sys_user_table
    /// </summary>
    public class Sys_user_table
    {
        private readonly DAL.Sys_user_table dal = new DAL.Sys_user_table();

        #region  BasicMethod

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(Model.Sys_user_table model)
        {
            return dal.Exists(model);
        }

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public bool Add(Model.Sys_user_table model)
        {
            return dal.Add(model);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool UpdateWidth(Model.Sys_user_table model)
        {
            return dal.UpdateWidth(model);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool UpdateHide(Model.Sys_user_table model)
        {
            return dal.UpdateHide(model);
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(Model.Sys_user_table model)
        {
            //该表无主键信息，请自定义主键/条件字段
            return dal.Delete(model);
        }


        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(Model.Sys_user_table model)
        {
            return dal.GetList(model);
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}