﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="CSS/swfupload.css" rel="stylesheet" />

    <link href="lib/ligerUI/skins/ext/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <script src="lib/jquery/jquery-1.3.2.min.js"></script>

    <script src="lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script>

    <script type="text/javascript" src="js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="js/swfupload/swfupload.queue.js"></script>
    <script type="text/javascript" src="js/swfupload/fileprogress.js"></script>
    <script type="text/javascript" src="js/swfupload/filegroupprogress.js"></script>
    <script type="text/javascript" src="js/swfupload/handlers.js"></script>

    <script src="lib/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"></script>
    <script type="text/javascript">
        var swfu;
        $(function () {

            toolbar();
            swfupload();


            $("#maingrid4").ligerGrid({
                columns: [
                        { display: '序号', width: 50, render: function (rowData, rowindex, value, column, rowid, page, pagesize) { return rowid + 1; } },

                        { display: '文件名', name: 'name', width: 250, align: 'left' },
                        {
                            display: '文件大小', name: 'size', width: 80, align: 'right', render: function (item, i) {
                                return formatUnits(item.size);
                            }
                        },
                        {
                            display: '类型', name: 'type', width: 40, render: function (item, i) {
                                var html = "<div style='width:18px;height:18px;margin:2px auto;' class=";
                                html += getType(item.type);
                                html += "></div>";
                                return html;
                            }
                        },
                        {
                            display: '进度', width: 100, name: 'percent', render: function (item, i) {
                                var html = "<div style='background:#0f0;";
                                html += "width:" + item.percent + "'>";
                                html += item.percent;
                                html += "%</div>";
                                return html;
                            }
                        },
                        {
                            display: '状态', name: 'filestatus', width: 80, render: function (item, i) {
                                //var html = "<div class='";
                                //switch (item.filestatus)
                                //{
                                //    case -1: html += "IcoWaiting"; break;
                                //    case -2: html += "IcoNormal"; break;
                                //    case -3: html += "IcoUpload"; break;
                                //}
                                //html+="' ></div>";
                                //return html;
                                switch (item.filestatus) {
                                    case -1: return "等待上传"; break;
                                    case -2: return "上传成功"; break;
                                    case -3: return "正在上传"; break;
                                }
                            }
                        }
                ],
                usePager: false, data: { Rows: [] },
                width: '100%', height: '100%',
                heightDiff: -32
            });
        })
        function test(file) {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = {
                filestatus: file.filestatus,
                id: file.id,
                type: file.type,
                creationdate: file.creationdate,
                modificationdate: file.modificationdate,
                post: file.post,
                name: file.name,
                size: file.size,
                index: file.index,
                percent: 0
            }
            manager.addRow(row);
        }
        function updateRow(file, percent) {
            var manager = $("#maingrid4").ligerGetGridManager();
            var rowObj = manager.getRow(file.index);
            var row = {
                filestatus: file.filestatus,
                id: file.id,
                type: file.type,
                creationdate: file.creationdate,
                modificationdate: file.modificationdate,
                post: file.post,
                name: file.name,
                size: file.size,
                index: file.index,
                percent: percent
            }
            manager.updateRow(file.index, row); //alert(JSON.stringify(rowObj));
        }

        function swfupload() {
            var settings = {
                flash_url: "js/swfupload/swfupload.swf",
                upload_url: "upload.contract.xhd",
                post_params: {
                    "ASPSESSID": "<%=Session.SessionID %>"
                },
                file_size_limit: "10 MB",
                file_types: "*.*",
                file_types_description: "All Files",
                file_upload_limit: 0,
                file_queue_limit: 0,
                custom_settings: {

                    progressTarget: "divprogresscontainer",
                    progressGroupTarget: "divprogressGroup",

                    //progress object
                    container_css: "progressobj",
                    icoNormal_css: "IcoNormal",
                    icoWaiting_css: "IcoWaiting",
                    icoUpload_css: "IcoUpload",
                    fname_css: "fle ftt",
                    state_div_css: "statebarSmallDiv",
                    state_bar_css: "statebar",
                    percent_css: "ftt",
                    href_delete_css: "ftt",

                    //sum object
                    /*
                        页面中不应出现以"cnt_"开头声明的元素
                    */
                    s_cnt_progress: "cnt_progress",
                    s_cnt_span_text: "fle",
                    s_cnt_progress_statebar: "cnt_progress_statebar",
                    s_cnt_progress_percent: "cnt_progress_percent",
                    s_cnt_progress_uploaded: "cnt_progress_uploaded",
                    s_cnt_progress_size: "cnt_progress_size"
                },
                debug: false,

                // Button settings
                //button_image_url: "images/xgcalendar/bot.gif",
                button_width: "55",
                button_height: "20",
                button_placeholder_id: "swf_add",
                button_text: '<span class="theFont">添加附件</span>',
                button_text_style: ".theFont { font-size: 12;color:#0068B7; }",
                button_text_left_padding: 3,
                button_text_top_padding: 1,
                button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
                button_cursor: SWFUpload.CURSOR.HAND,

                // The event handler functions are defined in handlers.js

                file_queued_handler: fileQueued,
                file_queue_error_handler: fileQueueError,
                upload_start_handler: uploadStart,
                upload_progress_handler: uploadProgress,
                upload_error_handler: uploadError,
                upload_success_handler: uploadSuccess,
                upload_complete_handler: uploadComplete,
                file_dialog_complete_handler: fileDialogComplete,
                queue_complete_handler: queueComplete
            };
        swfu = new SWFUpload(settings);
    }

    function getType(type) {
        type = type.replace('.', '')
        switch (type) {
            case "doc": case "wps": case "docx": return "doc";
            case "zip": case "rar": case "ace": case "7z": return "zip";
            case "swf": case "fla": return "swf";
            case "rmv": case "rm": case "wmv": case "avi": case "mpg": return "dvd";
            case "chm": case "pdf": return "book";
            case "ppt": case "pptx":return "ppt";
            case "xls": case "xlsx":return "xls";
            case "exe": case "bat": return "exe";
            case "cpp": case "js": case "jav": case "css": case "cs": case "h": case "cgi": return "scr";
            case "jpg": case "gif": case "png": case "psd": case "bmp": return "img";
            case "htm": case "xml": case "xht": case "sht": case "asp": case "jsp": case "php": case "txt": return "txt";
            case "cfg": case "dll": case "ini": return "cfg";
            case "mp3": case "wma": case "ape": case "wav": case "mid": return "mp3";
            default: return "oth";
        }
    }
    function toolbar() {
        var items = [];
        items.push({ type: 'button', text: '', expid: 'swf_add', icon: '../images/icon/11.png', disable: true });
        $("#toolbar").ligerToolBar({
            items: items

        });

    }
    </script>
</head>
<body style="background: #fff; overflow: hidden;">
    <form id="form1">
        <div id="toolbar"></div>
        <div id="maingrid4" style="margin: -1px;"></div>
        <div id="progress" class="l-toolbar"></div>
        <span id="spanButtonPlaceHolder"></span>
        <div id="divprogresscontainer1" style=""></div>
        <div id="divprogresscontainer"></div>
        <div id="divprogressGroup"></div>
        <input type="button" value="test" id="btn_up" onclick="test()" />
    </form>
</body>
</html>
