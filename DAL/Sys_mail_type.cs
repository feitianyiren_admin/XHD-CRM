﻿/*
* Sys_mail_type.cs
*
* 功 能： N/A
* 类 名： Sys_mail_type
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-07-20 09:50:37    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com  www.xhdoa.com. All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using XHD.DBUtility;

//Please add references

namespace XHD.DAL
{
    /// <summary>
    ///     数据访问类:Sys_mail_type
    /// </summary>
    public class Sys_mail_type
    {
        #region  BasicMethod

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(XHD.Model.Sys_mail_type model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Sys_mail_type(");
            strSql.Append("mail_type,use_ssl,pop3,pop3_port,imap,imap_port,smtp,smtp_port,mail_order)");
            strSql.Append(" values (");
            strSql.Append("@mail_type,@use_ssl,@pop3,@pop3_port,@imap,@imap_port,@smtp,@smtp_port,@mail_order)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@mail_type", SqlDbType.VarChar,250),
					new SqlParameter("@use_ssl", SqlDbType.Int,4),
					new SqlParameter("@pop3", SqlDbType.VarChar,250),
					new SqlParameter("@pop3_port", SqlDbType.Int,4),
					new SqlParameter("@imap", SqlDbType.VarChar,250),
					new SqlParameter("@imap_port", SqlDbType.Int,4),
					new SqlParameter("@smtp", SqlDbType.VarChar,250),
					new SqlParameter("@smtp_port", SqlDbType.Int,4),
					new SqlParameter("@mail_order", SqlDbType.Int,4)};
            parameters[0].Value = model.mail_type;
            parameters[1].Value = model.use_ssl;
            parameters[2].Value = model.pop3;
            parameters[3].Value = model.pop3_port;
            parameters[4].Value = model.imap;
            parameters[5].Value = model.imap_port;
            parameters[6].Value = model.smtp;
            parameters[7].Value = model.smtp_port;
            parameters[8].Value = model.mail_order;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(XHD.Model.Sys_mail_type model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Sys_mail_type set ");
            strSql.Append("mail_type=@mail_type,");
            strSql.Append("use_ssl=@use_ssl,");
            strSql.Append("pop3=@pop3,");
            strSql.Append("pop3_port=@pop3_port,");
            strSql.Append("imap=@imap,");
            strSql.Append("imap_port=@imap_port,");
            strSql.Append("smtp=@smtp,");
            strSql.Append("smtp_port=@smtp_port,");
            strSql.Append("mail_order=@mail_order");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
					new SqlParameter("@mail_type", SqlDbType.VarChar,250),
					new SqlParameter("@use_ssl", SqlDbType.Int,4),
					new SqlParameter("@pop3", SqlDbType.VarChar,250),
					new SqlParameter("@pop3_port", SqlDbType.Int,4),
					new SqlParameter("@imap", SqlDbType.VarChar,250),
					new SqlParameter("@imap_port", SqlDbType.Int,4),
					new SqlParameter("@smtp", SqlDbType.VarChar,250),
					new SqlParameter("@smtp_port", SqlDbType.Int,4),
					new SqlParameter("@mail_order", SqlDbType.Int,4),
					new SqlParameter("@id", SqlDbType.Int,4)};
            parameters[0].Value = model.mail_type;
            parameters[1].Value = model.use_ssl;
            parameters[2].Value = model.pop3;
            parameters[3].Value = model.pop3_port;
            parameters[4].Value = model.imap;
            parameters[5].Value = model.imap_port;
            parameters[6].Value = model.smtp;
            parameters[7].Value = model.smtp_port;
            parameters[8].Value = model.mail_order;
            parameters[9].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int id)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from Sys_mail_type ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     批量删除数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from Sys_mail_type ");
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append("select id,mail_type,use_ssl,pop3,pop3_port,imap,imap_port,smtp,smtp_port,mail_order ");
            strSql.Append(" FROM Sys_mail_type ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            var strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top);
            }
            strSql.Append(" id,mail_type,use_ssl,pop3,pop3_port,imap,imap_port,smtp,smtp_port,mail_order ");
            strSql.Append(" FROM Sys_mail_type ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            var strSql_grid = new StringBuilder();
            var strSql_total = new StringBuilder();
            strSql_total.Append(" SELECT COUNT(id) FROM Sys_mail_type ");
            strSql_grid.Append("SELECT ");
            strSql_grid.Append("      n,id,mail_type,use_ssl,pop3,pop3_port,imap,imap_port,smtp,smtp_port,mail_order ");
            strSql_grid.Append(
                " FROM ( SELECT id,mail_type,use_ssl,pop3,pop3_port,imap,imap_port,smtp,smtp_port,mail_order, ROW_NUMBER() OVER( Order by " +
                filedOrder + " ) AS n from Sys_mail_type");
            if (strWhere.Trim() != "")
            {
                strSql_grid.Append(" WHERE " + strWhere);
                strSql_total.Append(" WHERE " + strWhere);
            }
            strSql_grid.Append("  ) as w1  ");
            strSql_grid.Append("WHERE n BETWEEN " + PageSize*(PageIndex - 1) + " AND " + PageSize*PageIndex);
            strSql_grid.Append(" ORDER BY " + filedOrder);
            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}