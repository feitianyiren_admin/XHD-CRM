﻿/*
* hr_department.cs
*
* 功 能： N/A
* 类 名： hr_department
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-24 10:09:14    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using XHD.DBUtility;

//Please add references

namespace XHD.DAL
{
    /// <summary>
    ///     数据访问类:hr_department
    /// </summary>
    public class hr_department
    {
        #region  BasicMethod

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public int Add(Model.hr_department model)
        {
            var strSql = new StringBuilder();
            strSql.Append("insert into hr_department(");
            strSql.Append(
                "d_name,parentid,d_type,d_icon,d_fuzeren,d_tel,d_fax,d_add,d_email,d_miaoshu,d_order,isDelete,Delete_time)");
            strSql.Append(" values (");
            strSql.Append(
                "@d_name,@parentid,@d_type,@d_icon,@d_fuzeren,@d_tel,@d_fax,@d_add,@d_email,@d_miaoshu,@d_order,@isDelete,@Delete_time)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters =
            {
                new SqlParameter("@d_name", SqlDbType.VarChar, 50),
                new SqlParameter("@parentid", SqlDbType.Int, 4),
                new SqlParameter("@d_type", SqlDbType.VarChar, 50),
                new SqlParameter("@d_icon", SqlDbType.VarChar, 50),
                new SqlParameter("@d_fuzeren", SqlDbType.VarChar, 50),
                new SqlParameter("@d_tel", SqlDbType.VarChar, 50),
                new SqlParameter("@d_fax", SqlDbType.VarChar, 50),
                new SqlParameter("@d_add", SqlDbType.VarChar, 255),
                new SqlParameter("@d_email", SqlDbType.VarChar, 50),
                new SqlParameter("@d_miaoshu", SqlDbType.VarChar, 255),
                new SqlParameter("@d_order", SqlDbType.VarChar, 50),
                new SqlParameter("@isDelete", SqlDbType.Int, 4),
                new SqlParameter("@Delete_time", SqlDbType.DateTime)
            };
            parameters[0].Value = model.d_name;
            parameters[1].Value = model.parentid;
            parameters[2].Value = model.d_type;
            parameters[3].Value = model.d_icon;
            parameters[4].Value = model.d_fuzeren;
            parameters[5].Value = model.d_tel;
            parameters[6].Value = model.d_fax;
            parameters[7].Value = model.d_add;
            parameters[8].Value = model.d_email;
            parameters[9].Value = model.d_miaoshu;
            parameters[10].Value = model.d_order;
            parameters[11].Value = model.isDelete;
            parameters[12].Value = model.Delete_time;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            return Convert.ToInt32(obj);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.hr_department model)
        {
            var strSql = new StringBuilder();
            strSql.Append("update hr_department set ");
            strSql.Append("d_name=@d_name,");
            strSql.Append("parentid=@parentid,");
            strSql.Append("d_type=@d_type,");
            strSql.Append("d_icon=@d_icon,");
            strSql.Append("d_fuzeren=@d_fuzeren,");
            strSql.Append("d_tel=@d_tel,");
            strSql.Append("d_fax=@d_fax,");
            strSql.Append("d_add=@d_add,");
            strSql.Append("d_email=@d_email,");
            strSql.Append("d_miaoshu=@d_miaoshu,");
            strSql.Append("d_order=@d_order");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@d_name", SqlDbType.VarChar, 50),
                new SqlParameter("@parentid", SqlDbType.Int, 4),
                new SqlParameter("@d_type", SqlDbType.VarChar, 50),
                new SqlParameter("@d_icon", SqlDbType.VarChar, 50),
                new SqlParameter("@d_fuzeren", SqlDbType.VarChar, 50),
                new SqlParameter("@d_tel", SqlDbType.VarChar, 50),
                new SqlParameter("@d_fax", SqlDbType.VarChar, 50),
                new SqlParameter("@d_add", SqlDbType.VarChar, 255),
                new SqlParameter("@d_email", SqlDbType.VarChar, 50),
                new SqlParameter("@d_miaoshu", SqlDbType.VarChar, 255),
                new SqlParameter("@d_order", SqlDbType.VarChar, 50),
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.d_name;
            parameters[1].Value = model.parentid;
            parameters[2].Value = model.d_type;
            parameters[3].Value = model.d_icon;
            parameters[4].Value = model.d_fuzeren;
            parameters[5].Value = model.d_tel;
            parameters[6].Value = model.d_fax;
            parameters[7].Value = model.d_add;
            parameters[8].Value = model.d_email;
            parameters[9].Value = model.d_miaoshu;
            parameters[10].Value = model.d_order;
            parameters[11].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int id)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from hr_department ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     批量删除数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from hr_department ");
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append(
                "select id,d_name,parentid,d_type,d_icon,d_fuzeren,d_tel,d_fax,d_add,d_email,d_miaoshu,d_order,isDelete,Delete_time ");
            strSql.Append(
                " ,(select d_name from hr_department w1 where w1.id = hr_department.parentid ) as parentname  ");
            strSql.Append(" FROM hr_department ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            var strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top);
            }
            strSql.Append(
                " id,d_name,parentid,d_type,d_icon,d_fuzeren,d_tel,d_fax,d_add,d_email,d_miaoshu,d_order,isDelete,Delete_time ");
            strSql.Append(
                " ,(select d_name from hr_department w1 where w1.id = hr_department.parentid ) as parentname  ");
            strSql.Append(" FROM hr_department ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            var strSql_grid = new StringBuilder();
            var strSql_total = new StringBuilder();
            strSql_total.Append(" SELECT COUNT(id) FROM hr_department ");
            strSql_grid.Append("SELECT ");
            strSql_grid.Append(
                "      id,d_name,parentid,d_type,d_icon,d_fuzeren,d_tel,d_fax,d_add,d_email,d_miaoshu,d_order,isDelete,Delete_time ");
            strSql_grid.Append(" ,(select d_name from hr_department w2 where w2.id = w1.parentid ) as parentname  ");
            strSql_grid.Append(
                " FROM ( SELECT id,d_name,parentid,d_type,d_icon,d_fuzeren,d_tel,d_fax,d_add,d_email,d_miaoshu,d_order,isDelete,Delete_time, ROW_NUMBER() OVER( Order by " +
                filedOrder + " ) AS n from hr_department");
            if (strWhere.Trim() != "")
            {
                strSql_grid.Append(" WHERE " + strWhere);
                strSql_total.Append(" WHERE " + strWhere);
            }
            strSql_grid.Append("  ) as w1  ");
            strSql_grid.Append("WHERE n BETWEEN " + PageSize*(PageIndex - 1) + " AND " + PageSize*PageIndex);
            strSql_grid.Append(" ORDER BY " + filedOrder);
            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}