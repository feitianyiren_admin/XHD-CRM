﻿/*
* Sys_data_authority.cs
*
* 功 能： N/A
* 类 名： Sys_data_authority
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Web;
using XHD.Common;
using XHD.Controller;
using System.Web.Script.Serialization;
using System.Collections.Generic;

namespace XHD.Server
{
    public class Sys_data_authority
    {
        public static BLL.Sys_data_authority auth = new BLL.Sys_data_authority();
        public static Model.Sys_data_authority model = new Model.Sys_data_authority();
        

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public Sys_data_authority()
        {
        }

        public Sys_data_authority(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public string get(int Role_id)
        {
            DataSet ds = auth.GetList("Role_id=" + Role_id);
            if (ds.Tables[0].Rows.Count == 4)
            {
                return (GetGridJSON.DataTableToJSON(ds.Tables[0]));
            }
            string datatxt = "";

            datatxt += "{Rows: [";
            datatxt +="        { '__status': null, 'option_id': 1, 'Sys_option': '客户管理', 'Sys_view': 1, 'Sys_add': 1, 'Sys_edit': 1, 'Sys_del': 1 },";
            datatxt +="        { '__status': null, 'option_id': 2, 'Sys_option': '跟进管理', 'Sys_view': 1, 'Sys_add': 1, 'Sys_edit': 1, 'Sys_del': 1 },";
            datatxt +="        { '__status': null, 'option_id': 3, 'Sys_option': '订单管理', 'Sys_view': 1, 'Sys_add': 1, 'Sys_edit': 1, 'Sys_del': 1 },";
            datatxt +="        { '__status': null, 'option_id': 4, 'Sys_option': '合同管理', 'Sys_view': 1, 'Sys_add': 1, 'Sys_edit': 1, 'Sys_del': 1 }";
            datatxt += "    ],Total: 4 }";
            return (datatxt);
        }

        public void save(int id, string savestring)
        {
            model.Role_id = id;

            auth.Delete("Role_id=" + id);

            var json = new JavaScriptSerializer();
            var _list = json.Deserialize<List<AuthData>>(savestring);

            foreach (AuthData authdata in _list)
            {
                model.option_id = authdata.option_id;
                model.Sys_view = authdata.Sys_view;
                model.Sys_add = authdata.Sys_add;
                model.Sys_edit = authdata.Sys_edit;
                model.Sys_del = authdata.Sys_del;
                model.Sys_option = PageValidate.InputText(authdata.Sys_option, 50);

                auth.Add(model);
            }
        }

        public class AuthData
        {
            public int option_id { get; set; }
            public string Sys_option { get; set; }
            public int Sys_view { get; set; }
            public int Sys_add { get; set; }
            public int Sys_edit { get; set; }
            public int Sys_del { get; set; }
        }
    }
}