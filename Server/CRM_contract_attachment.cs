﻿/*
* CRM_contract_attachment.cs
*
* 功 能： N/A
* 类 名： CRM_contract_attachment
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.IO;
using System.Web;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class CRM_contract_attachment
    {
        public static BLL.CRM_contract_attachment atta = new BLL.CRM_contract_attachment();
        public static Model.CRM_contract_attachment model = new Model.CRM_contract_attachment();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public CRM_contract_attachment()
        {
        }

        public CRM_contract_attachment(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public void insert_attachment()
        {
            model.file_id = PageValidate.InputText(request["file_id"], 255);
            model.file_name = PageValidate.InputText(request["file_name"], 255);
            model.real_name = PageValidate.InputText(request["real_name"], 255);
            model.page_id = PageValidate.InputText(request["page_id"], 255);
            model.file_size = int.Parse(request["file_size"]);

            if (PageValidate.IsNumber(request["contract_id"]))
                model.contract_id = int.Parse(request["contract_id"]);

            model.create_id = emp_id;
            model.create_name = emp_name;
            model.create_date = DateTime.Now;

            atta.Add(model);
        }

        public void flush_attachment(string page_id)
        {
            page_id = PageValidate.InputText(page_id, 50);
            DataSet attachment = atta.GetList("page_id='" + page_id + "'");
            atta.Delete("page_id='" + page_id + "'");

            for (int i = 0; i < attachment.Tables[0].Rows.Count; i++)
            {
                try
                {
                    string filename = attachment.Tables[0].Rows[i]["real_name"].ToString();
                    string sExt = filename.Substring(filename.LastIndexOf('.'));
                    string html = filename.Substring(0, filename.Length - sExt.Length) + ".html";
                    string files = filename.Substring(0, filename.Length - sExt.Length) + ".files";
                    File.Delete(HttpContext.Current.Server.MapPath("../file/contract/" + filename));
                    File.Delete(HttpContext.Current.Server.MapPath("../file/contract/" + html));

                    var dir = new DirectoryInfo(HttpContext.Current.Server.MapPath("../file/contract/" + files));
                    dir.Delete(true);
                }
                catch
                {
                }
            }
        }

        public void del_attachment(string file_id, string page_id)
        {
            file_id = PageValidate.InputText(file_id, 255);
            page_id = PageValidate.InputText(page_id, 50);

            DataSet attachment = atta.GetList("file_id='" + file_id + "' and page_id='" + page_id + "'");
            atta.Delete("file_id='" + file_id + "' and page_id='" + page_id + "'");

            for (int i = 0; i < attachment.Tables[0].Rows.Count; i++)
            {
                try
                {
                    File.Delete(HttpContext.Current.Server.MapPath("../file/contract/" +attachment.Tables[0].Rows[i]["real_name"]));
                }
                catch
                {
                }
            }
        }

        public string get_attachment(string contract_id, string page_id)
        {
            string whereStr = "";
            contract_id = PageValidate.InputText(contract_id, 50);
            page_id = PageValidate.InputText(page_id, 50);

            if (PageValidate.IsNumber(contract_id))
                whereStr = string.Format(" contract_id={0}", contract_id);
            else if (PageValidate.IsNumber(page_id))
                whereStr = string.Format(" page_id={0}", page_id);
            else
                whereStr = " 1=2 ";


            DataSet ds = atta.GetList(whereStr);
            string dt = GetGridJSON.DataTableToJSON(ds.Tables[0]);

            return (dt);
        }

        public string get_realname(string page_id, string file_name)
        {
            page_id = PageValidate.InputText(page_id, 50);
            file_name = PageValidate.InputText(file_name, 255);

            DataSet ds = atta.GetList(string.Format("page_id='{0}' and file_name='{1}'", page_id, file_name));

            if (ds.Tables[0].Rows.Count > 0)
                return (ds.Tables[0].Rows[0]["real_name"].ToString());
            return ("sucess:false");
        }

        public string get_office(string real_name, string page_id, string file_name)
        {
            real_name = PageValidate.InputText(real_name, 255);
            page_id = PageValidate.InputText(page_id, 255);
            file_name = PageValidate.InputText(file_name, 255);

            if (string.IsNullOrEmpty(real_name) || real_name == "undefined")
            {
                DataSet ds = atta.GetList(string.Format("page_id='{0}' and file_name='{1}'", page_id, file_name));

                if (ds.Tables[0].Rows.Count > 0)
                    real_name = ds.Tables[0].Rows[0]["real_name"].ToString();
                else
                    real_name = null;
            }

            if (!string.IsNullOrEmpty(real_name))
            {
                string filename = Context.Server.MapPath(@"../file/contract/" + real_name);
                //string readname = WordToHtml(filename);
                //StreamReader fread = new StreamReader(readname, System.Text.Encoding.GetEncoding("gb2312"));
                //string ss = fread.ReadToEnd();
                //string sExt = readname.Substring(readname.LastIndexOf("\\")).ToLower();
                //real_name = real_name.Replace(sExt, "");
                return (filename);
                //fread.Close();
                //fread.Dispose();
            }
            return null;
        }
    }
}