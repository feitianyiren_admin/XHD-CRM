﻿/*
* Param_City.cs
*
* 功 能： N/A
* 类 名： Param_City
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using System.Text;
using System.Web;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class Param_City
    {
        public static BLL.Param_City pc = new BLL.Param_City();
        public static Model.Param_City model = new Model.Param_City();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public Param_City()
        {
        }

        public Param_City(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public string treegrid()
        {
            DataSet ds = pc.GetList(0, "", "City_order");
            string dt = "{Rows:[" + GetTasks.GetTasksString(0, ds.Tables[0]) + "]}";
            return dt;
        }

        public string tree()
        {
            DataSet ds = pc.GetAllList();
            var str = new StringBuilder();
            str.Append("[");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                str.Append("{id:" + ds.Tables[0].Rows[i]["id"] + ",pid:" + ds.Tables[0].Rows[i]["parentid"] + ",text:'" +
                           ds.Tables[0].Rows[i]["City"] + "'},");
            }
            str.Replace(",", "", str.Length - 1, 1);
            str.Append("]");
            return str.ToString();
        }

        //save
        public void save()
        {
            model.City = PageValidate.InputText(request["T_City"], 255);
            model.City_order = int.Parse(request["T_order"]);
            string pid = request["T_Parent_val"];
            if (string.IsNullOrEmpty(pid))
            {
                pid = "0";
            }
            model.parentid = int.Parse(pid);

            string id = PageValidate.InputText(request["id"], 50);

            if (!string.IsNullOrEmpty(id) && id != "null")
            {
                model.id = int.Parse(id);
                pc.Update(model);
            }
            else
            {
                pc.Add(model);
            }
        }

        //Form JSON
        public string form(string id)
        {
            string dt;

            if (PageValidate.IsNumber(id))
            {
                DataSet ds = pc.GetList("id=" + id);

                dt = DataToJson.DataToJSON(ds);
            }
            else
            {
                dt = "{}";
            }

            return dt;
        }

        //del
        public string del(int id)
        {
            DataSet ds = pc.GetList(" parentid=" + id);
            var cus = new BLL.CRM_Customer();
            DataSet ds1 = cus.GetList(string.Format("Provinces_id={0} or City_id={0}", id));
            if (ds.Tables[0].Rows.Count > 0)
            {
                return ("false:parent");
            }
            if (ds1.Tables[0].Rows.Count > 0)
            {
                return ("false:customer");
            }
            bool isdel = pc.Delete(id);
            if (isdel)
            {
                return ("true");
            }
            return ("false");
        }

        public string combo()
        {
            DataSet ds = pc.GetList("parentid=0");

            var str = new StringBuilder();

            str.Append("[");
            str.Append("{id:0,text:'无'},");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                str.Append("{id:" + ds.Tables[0].Rows[i]["id"] + ",text:'" + ds.Tables[0].Rows[i]["City"] + "'},");
            }
            str.Replace(",", "", str.Length - 1, 1);
            str.Append("]");

            return str.ToString();
        }

        public string combo1()
        {
            DataSet ds = pc.GetList("parentid=0");

            var str = new StringBuilder();

            str.Append("[");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                str.Append("{id:" + ds.Tables[0].Rows[i]["id"] + ",text:'" + ds.Tables[0].Rows[i]["City"] + "'},");
            }
            str.Replace(",", "", str.Length - 1, 1);
            str.Append("]");

            return str.ToString();
        }

        public string combo2()
        {
            int id = int.Parse(request["pid"]);
            DataSet ds = pc.GetList("parentid=" + id);

            var str = new StringBuilder();

            str.Append("[");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                str.Append("{id:" + ds.Tables[0].Rows[i]["id"] + ",text:'" + ds.Tables[0].Rows[i]["City"] + "'},");
            }
            str.Replace(",", "", str.Length - 1, 1);
            str.Append("]");

            return str.ToString();
        }
    }
}