﻿/*
* Personal_notes.cs
*
* 功 能： N/A
* 类 名： Personal_notes
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Web;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class Personal_notes
    {
        public static BLL.Personal_notes notes = new BLL.Personal_notes();
        public static Model.Personal_notes model = new Model.Personal_notes();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public Personal_notes()
        {
        }

        public Personal_notes(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public string Get()
        {
            DataSet ds = notes.GetList("emp_id=" + emp_id);

            return (GetGridJSON.DataTableToJSON2(ds.Tables[0]));
        }

        public int save()
        {
            model.emp_id = emp_id;
            model.note_content = PageValidate.InputText(request["body"], int.MaxValue);
            model.note_time = DateTime.Now;
            model.note_color = PageValidate.InputText(request["color"], 50);
            model.xyz = decimal.Parse(request["left"]) + "," + decimal.Parse(request["top"]) + "," +
                        decimal.Parse(request["zindex"]);

            int id = notes.Add(model);

            return id;
        }

        public void update()
        {
            model.xyz = decimal.Parse(request["x"]) + "," + decimal.Parse(request["y"]) + "," +
                        decimal.Parse(request["z"]);
            model.id = int.Parse(request["id"]);

            notes.Update(model);
        }

        public string delete(int id)
        {
            bool a = notes.Delete(id);
            return a.ToString();
        }

        public string grid()
        {
            DataSet ds = notes.GetList(0, "emp_id=" + emp_id, "note_time desc");
            DataTable dt = ds.Tables[0];

            return (GetGridJSON.DataTableToJSON(dt));
        }

        public string notesremind()
        {
            DataSet ds = notes.GetList(7, "emp_id=" + emp_id, " note_time desc");
            string dt = GetGridJSON.DataTableToJSON(ds.Tables[0]);
            return dt;
        }
    }
}