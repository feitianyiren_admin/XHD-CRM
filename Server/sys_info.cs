﻿/*
* sys_info.cs
*
* 功 能： N/A
* 类 名： sys_info
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Web;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class sys_info
    {
        public static BLL.sys_info info = new BLL.sys_info();
        public static Model.sys_info model = new Model.sys_info();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public sys_info()
        {
        }

        public sys_info(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public string grid()
        {
            DataSet ds = info.GetAllList();
            return (GetGridJSON.DataTableToJSON(ds.Tables[0]));
        }

        public void up()
        {
            model.sys_value = PageValidate.InputText(request["T_name"], int.MaxValue);
            model.sys_key = "sys_name";

            info.Update(model);
        }

        public void logo()
        {
            string fileName = request["filename"]; //文件路径
            fileName = fileName.Substring(fileName.LastIndexOf('\\') + 1);
            string sExt = fileName.Substring(fileName.LastIndexOf(".")).ToLower();

            DateTime now = DateTime.Now;
            string nowfileName = now.ToString("yyyyMMddHHmmss") + Assistant.GetRandomNum(6) + sExt;

            HttpPostedFile uploadFile = request.Files[0];
            uploadFile.SaveAs(Context.Server.MapPath(@"~/images/logo/" + nowfileName));

            //context.Response.Write(nowfileName);
            model.sys_value = "images/logo/" + nowfileName;
            model.sys_key = "sys_logo";

            info.Update(model);
        }

        public string regSMS()
        {
            string SerialNo = PageValidate.InputText(request["T_SerialNo"], 50);
            model.sys_value = SerialNo;
            model.sys_key = "sms_no";
            info.Update(model);

            string key = PageValidate.InputText(request["T_key"], 50);
            model.sys_value = Common.DEncrypt.DESEncrypt.Encrypt(key); 
            model.sys_key = "sms_key";
            info.Update(model);

            SMS.SMSHelper sms = new SMS.SMSHelper();
            int v = sms.registEx(SerialNo, key, key);

            if (v == 0)
            {
                model.sys_value = "1";
                model.sys_key = "sms_done";
                info.Update(model);
            }

            var sms_result = SMS.SMSHelper.sms_result(v);
            return sms_result;
        }
    }
}