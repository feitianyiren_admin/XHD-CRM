﻿/*
* login.cs
*
* 功 能： N/A
* 类 名： login
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using XHD.BLL;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class login : IRequiresSessionState
    {
        public HttpContext Context;
        public HttpRequest request;

        public login()
        {
        }

        public login(HttpContext context)
        {
            Context = context;
            request = context.Request;
        }

        public string check()
        {
            var emp = new BLL.hr_employee();

            string username = PageValidate.InputText(request["username"], 255);
            //string password = FormsAuthentication.HashPasswordForStoringInConfigFile(request["password"], "MD5");
            string password = PageValidate.InputText(request["password"], 255);
            string validate = PageValidate.InputText(request["validate"], 255);

            string checkcode = "";
            try
            {
                checkcode = GetSession.session["CheckCode"].ToString();
            }
            catch
            {
                return "5";//验证码不存在或过期
            }

            if (!string.IsNullOrEmpty(validate) && !string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
            {
                if (validate.ToLower() != checkcode.ToLower())
                    return ("0"); //验证码错误

                DataSet ds = emp.GetList(" uid='" + username + "' and pwd='" + password + "'");
                if (ds.Tables[0].Rows.Count == 0)
                    return ("1"); //用户名或密码错误

                if (ds.Tables[0].Rows[0]["canlogin"].ToString() != "1")
                    return ("4"); //不允许登录

                string userid = ds.Tables[0].Rows[0]["ID"].ToString();
                var ticket = new FormsAuthenticationTicket(
                    1,
                    username,
                    DateTime.Now,
                    DateTime.Now.AddMinutes(20),
                    true,
                    userid,
                    "/"
                    );
                var cookie = new HttpCookie(FormsAuthentication.FormsCookieName,
                    FormsAuthentication.Encrypt(ticket));
                cookie.HttpOnly = true;
                Context.Response.Cookies.Add(cookie);

                //FormsAuthentication.SetAuthCookie(userid, true);

                //日志
                var log = new BLL.Sys_log();
                var modellog = new Model.Sys_log();
                modellog.EventType = "系统登录";

                modellog.EventDate = DateTime.Now;
                modellog.UserID = int.Parse(userid);
                modellog.IPStreet = request.UserHostAddress;

                log.Add(modellog);

                //online
                var sol = new Sys_online();
                var model = new Model.Sys_online();

                model.UserName = ds.Tables[0].Rows[0]["name"].ToString();
                model.UserID = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                model.LastLogTime = DateTime.Now;

                DataSet ds1 = sol.GetList(" UserID=" + userid);

                //添加当前用户信息
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    sol.Update(model, " UserID=" + userid);
                }
                else
                {
                    sol.Add(model);
                }
                //删除超时用户
                sol.Delete(" LastLogTime<DATEADD(MI,-1,getdate())");

                //验证完毕，允许登录
                return ("2");

            }
            return ("999"); //系统数据错误
        }

        public void logout()
        {
            HttpCookie cookie = Context.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (null != cookie)
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
                string CoockiesID = ticket.UserData;

                FormsAuthentication.SignOut();
                //Context.Response.Write("true");

                //online
                var sol = new Sys_online();
                try
                {
                    if (!string.IsNullOrEmpty(CoockiesID))
                    {
                        sol.Delete(" UserID=" + int.Parse(CoockiesID));
                    }
                }
                catch
                {
                }
            }
        }

        public string checkpwd()
        {
            HttpCookie cookie = Context.Request.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
            string CoockiesID = ticket.UserData;

            var emp = new BLL.hr_employee();

            int emp_id = int.Parse(CoockiesID);
            string password = FormsAuthentication.HashPasswordForStoringInConfigFile(request["password"], "MD5");


            DataSet ds = emp.GetList(string.Format("ID={0} and pwd='{1}'", emp_id, password));

            if (ds.Tables[0].Rows.Count > 0)
            {
                return ("{sucess:sucess}");
            }
            return ("{sucess:false}");
        }
    }
}