﻿/*
* Sys_mail_list.cs
*
* 功 能： N/A
* 类 名： Sys_mail_list
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using System.Web;
using XHD.Common;
using XHD.Controller;
using System.Text;

namespace XHD.Server
{
    public class Sys_mail_list
    {
        public static BLL.Sys_mail_list maillist = new BLL.Sys_mail_list();
        public static Model.Sys_mail_list model = new Model.Sys_mail_list();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public Sys_mail_list()
        {
        }

        public Sys_mail_list(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public void save()
        {
            model.email = PageValidate.InputText(request["T_email"], 50);
            string pwd=PageValidate.InputText(request["T_pwd"], 50);
            model.password = Common.DEncrypt.DESEncrypt.Encrypt(pwd);

            if (PageValidate.IsNumber(request["T_type_val"]))
                model.mail_type_id = int.Parse(request["T_type_val"]);

            if (PageValidate.IsNumber(request["T_employee_val"]))
                model.belong_id = int.Parse(request["T_employee_val"]);

            if (PageValidate.IsNumber(request["T_enable_val"]))
                model.mail_enable = int.Parse(request["T_enable_val"]);

            string id = PageValidate.InputText(request["id"], 50);
            if (PageValidate.IsNumber(id))
            {
                model.id = int.Parse(id);

                DataSet ds = maillist.GetList(string.Format("id={0}", id));
                DataRow dr = ds.Tables[0].Rows[0];

                maillist.Update(model);

                //日志
                var log = new sys_log();

                int UserID = emp_id;
                string UserName = emp_name;
                string IPStreet = request.UserHostAddress;
                string EventTitle = model.email;
                ;
                string EventType = "系统邮箱修改";
                int EventID = model.id;
                string Log_Content = null;

                if (dr["email"].ToString() != request["T_email"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "账号", dr["email"], request["T_email"]);

                if (dr["password"].ToString() != request["T_pwd"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "密码修改", "原密码", "");

                if (dr["mail_type_id"].ToString() != request["T_type_val"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "邮箱类型", dr["mail_type"], request["T_type"]);

                if (dr["mail_enable"].ToString() != request["T_enable_val"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "启用", dr["mail_enable"], request["T_enable_val"]);

                if (dr["belong_id"].ToString() != request["T_employee_val"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "归属", dr["belong"], request["T_employee"]);

                if (!string.IsNullOrEmpty(Log_Content))
                    log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, Log_Content);
            }
            else
            {
                maillist.Add(model);
            }
        }

        public string grid()
        {
            int PageIndex = int.Parse(request["page"] == null ? "1" : request["page"]);
            int PageSize = int.Parse(request["pagesize"] == null ? "30" : request["pagesize"]);
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];

            if (string.IsNullOrEmpty(sortname))
                sortname = " id ";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = " desc";

            string sorttext = " " + sortname + " " + sortorder;

            string Total;
            string serchtxt = "1=1";


            //if (!string.IsNullOrEmpty(request["customerid"]))
            //    serchtxt += " and C_customerid=" + int.Parse(request["customerid"]);

            //context.Response.Write(serchtxt);

            DataSet ds = maillist.GetList(PageSize, PageIndex, serchtxt, sorttext, out Total);

            string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], Total);

            return dt;
        }

        public string form(string id)
        {
            string dt;
            if (PageValidate.IsNumber(id))
            {
                DataSet ds = maillist.GetList(string.Format("id = {0}", int.Parse(id)));
                if (ds.Tables[0].Rows.Count > 0)
                    ds.Tables[0].Rows[0]["password"] = Common.DEncrypt.DESEncrypt.Decrypt(ds.Tables[0].Rows[0]["password"].ToString());
                dt = DataToJson.DataToJSON(ds);
            }
            else dt = "{}";

            return dt;
        }

        public string del(int id)
        {
            DataSet ds = maillist.GetList(string.Format("id = {0}", id));
            string EventType = "系统邮箱删除";

            bool isdel = maillist.Delete(id);
            if (isdel)
            {
                //日志
                int UserID = emp_id;
                string UserName = emp_name;
                string IPStreet = request.UserHostAddress;
                int EventID = id;
                string EventTitle = ds.Tables[0].Rows[0]["mail_type"].ToString();

                var log = new sys_log();
                log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID,
                    ds.Tables[0].Rows[0]["email"].ToString());

                return "true";
            }
            return "false";
        }

    }
}
