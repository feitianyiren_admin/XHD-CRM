/*
* sys_info.cs
*
* 功 能： N/A
* 类 名： sys_info
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     sys_info:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class sys_info
    {
        #region Model

        private int _id;
        private string _sys_key;
        private string _sys_value;

        /// <summary>
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }

        /// <summary>
        /// </summary>
        public string sys_key
        {
            set { _sys_key = value; }
            get { return _sys_key; }
        }

        /// <summary>
        /// </summary>
        public string sys_value
        {
            set { _sys_value = value; }
            get { return _sys_value; }
        }

        #endregion Model
    }
}